# DEEP NEURAL NETWORK
![](dnn.png)


**Neural network**
- A neural network is a network or circuit of neurons, or in a modern sense, an artificial neural network, composed of artificial neurons or nodes.

**Deep Neural network**
- A deep neural network is a neural network with a certain level of complexity, a neural network with more than two layers.
Deep neural networks use sophisticated mathematical modeling to process data in complex ways.

- A neural network is a series of algorithms that endeavors to recognize underlying relationships in a set of data through a process that mimics the way the human brain operates. In this sense, neural networks refer to systems of neurons, either organic or artificial in nature. Neural networks can adapt to changing input; so the network generates the best possible result without needing to redesign the output criteria.


- A neural network works similarly to the human brain’s neural network. A “neuron” in a neural network is a mathematical function that collects and classifies information according to a specific architecture. The network bears a strong resemblance to statistical methods such as curve fitting and regression analysis.


- A neural network contains layers of interconnected nodes. Each node is a perceptron and is similar to a multiple linear regression. The perceptron feeds the signal produced by a multiple linear regression into an activation function that may be nonlinear.


- In a multi-layered perceptron (MLP), perceptrons are arranged in interconnected layers. The input layer collects input patterns. The output layer has classifications or output signals to which input patterns may map. For instance, the patterns may comprise a list of quantities for technical indicators about a security; potential outputs could be “buy,” “hold” or “sell.”


- Hidden layers fine-tune the input weightings until the neural network’s margin of error is minimal. It is hypothesized that hidden layers extrapolate salient features in the input data that have predictive power regarding the outputs. This describes feature extraction, which accomplishes a utility similar to statistical techniques such as principal component analysis.


- They are used in a variety of applications in financial services, from forecasting and marketing research to fraud detection and risk assessment.

![](dnn_working.JPG)

- The input is fed to the input layer, the neurons perform a linear transformation on this input using the weights and biases.
**x = (weight * input) + bias**

- Post that, an activation function is applied on the above result.
**Y=Activation(Σ (weight * input) + bias)**

- Finally, the output from the activation function moves to the next hidden layer and the same process is repeated.
This forward movement of information is known as the **forward propagation.**

![](forward_propogaion.JPG)

- Using the output from the forward propagation, error is calculated. Based on this error value, the weights and biases of the neurons are updated. This process is known as **back-propagation**.

![](backward_propogation.JPG)

## Activation Functions

### Sigmoid
- It is one of the most widely used non-linear activation function.
- Sigmoid transforms the values between the range 0 and 1.
- Here is the mathematical expression for sigmoid- **f(x) = 1/(1+e^-x)**

### ReLU
- The ReLU function is another non-linear activation function that has gained popularity in the deep learning domain.
- ReLU stands for Rectified Linear Unit.
- The main advantage of using the ReLU function over other activation functions is that it does not activate all the neurons at the same time.
- This means that the neurons will only be deactivated if the output of the linear transformation is less than 0. **f(x)=max(0,x)**

## Cost Function
- It is a function that measures the performance of a Machine Learning model for given data.
- Cost Function quantifies the error between predicted values and expected values and presents it in the form of a single real number
- The cost function minimizes the value of the funtion

## Gradient Descent
- Gradient of the value gives the direction of steepest ascent.
- Gradient descent is an efficient optimization algorithm that attempts to find a local or global minima of a function.
- Gradient descent enables a model to learn the gradient or direction that the model should take in order to reduce errors.
