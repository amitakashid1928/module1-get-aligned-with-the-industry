# Product 1

## Product name
Face Track

## Product link
https://visagetechnologies.com/facetrack

## Product Short description
Face Track detects and tracks one or more faces and their facial features in images and videos from any standard camera or video file in color, gray-scale and near-infrared. For each detected face it returns detailed face data including:

- 2D and 3D head pose and facial points coordinates (chin tip, nose tip, lip corners etc.)
- A set of action units describing the current facial expressions (e.g. jaw drop)
- Eye closure and eye-gaze information
- 3D triangle mesh model of the face in the current pose and expression

## Product is combination of features
- Emotion Detection
- Pose Estimation

## Product is provided by which company?
Visage Technologies

# Product 2

## Product name
Vision AI

## Product link
https://cloud.google.com/vision/

## Product Short description
This tool, part of the Google Cloud Platform, enables developers to integrate image recognition and object detection capabilities using simple API (Application Programming Interface) calls. Google is leading the way in computer vision and you can be sure that'll benefit from the most powerful vision machine learning models using this tool. It can extract useful insights and text out of the examined images as well as integrate the functions of their reverse image search engine making it one of the most versatile and flexible tools.

## Product is combination of features
- Object detection
- Person Detection
- ID Recognition

## Product is provided by which company?
Google

# Product 3

## Product name
PoseMatch2

## Product link
https://play.google.com/store/apps/details?id=com.gil.posematchv3&hl=en_IN

## Product Short description
Human Pose Matching demo.
Using Human pose estimation and object detection.
A nice example of Deep Learning in the field of Computer Vision.
Actions that it can detect are:

- sitting,
- standing,
- raising hand,etc.

## Product is combination of features
- Pose Estimation

## Product is provided by which company?
BilGeckers


# Product 4

## Product name
Amazon Rekognition

## Product link
https://aws.amazon.com/rekognition/?blog-cards.sort-by=item.additionalFields.createdDate&blog-cards.sort-order=desc

## Product Short description
Amazon Rekognition makes it easy to add image and video analysis to your applications using proven, highly scalable, deep learning technology that requires no machine learning expertise to use. With Amazon Rekognition, you can identify objects, people, text, scenes, and activities in images and videos, as well as detect any inappropriate content. Amazon Rekognition also provides highly accurate facial analysis and facial search capabilities that you can use to detect, analyze, and compare faces for a wide variety of user verification, people counting, and public safety use cases.

## Product is combination of features
- Object detection
- Person Detection

## Product is provided by which company?
Amazon
